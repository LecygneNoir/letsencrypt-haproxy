# letsencrypt-haproxy

Some scripts to create and renew all your certificates, and concatenate fullchain and privkey so haproxy is able to use it

**For this to work, you should have configured haproxy to accept let's encrypt connection**  
More information [here](http://blog.victor-hery.com/index.php?article23/use-haproxy-with-let-s-encrypt)
([French version](http://blog.victor-hery.com/index.php?article22/utiliser-let-s-encrypt-avec-haproxy)

## Prerequisites

You need to install certbot according to your distribution :

For debian : 
```
apt-get install certbot
```

## How to create certificate
Create a certificate with let's encrypt, then concatenate fullchain.pem and privkey.pem, then store the certificate in haproxy given path.

Usage :  
```
./create-certificate domain.tld >> /var/log/create-certificate.log
```

## How to renew certificates
**Renew all certificate known by let's encrypt !**

Usage :  
```
./renew-certificate >> /var/log/renew-certificates.log
```

##How to create postfix SAN certificate
Create SAN (multi-domains) certificates for a postfix configuration

Edit postfix_pem_path  and mail_server and deploy ssh key accordingly.  
I should write a blog article once I have time, meanwhile do not hesitate to ask questions on the repo
Usage:
```
./create-cert-postfix domain1.tld domain2.tld domain3.tld
```